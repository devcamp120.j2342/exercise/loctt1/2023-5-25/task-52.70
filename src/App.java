import java.util.ArrayList;
import java.util.Date;

import models.Person;

public class App {
    public static void main(String[] args) throws Exception {
         // Khởi tạo đối tượng Order2
         Person buyer = new Person("John Doe");
         String[] items = {"Item 1", "Item 2", "Item 3"};
         Date orderDate = new Date();
 
         Order2 order1 = new Order2(1, "Customer 1", 1000, orderDate, true, items, buyer);
         Order2 order2 = new Order2(2, "Customer 2", 2000, orderDate, false, items, buyer);
         Order2 order3 = new Order2(3, "Customer 3", 3000, orderDate, true, items, buyer);
         Order2 order4 = new Order2(4, "Customer 4", 4000, orderDate, false, items, buyer);
 
         // Thêm các đối tượng Order2 vào ArrayList
         ArrayList<Order2> orders = new ArrayList<>();
         orders.add(order1);
         orders.add(order2);
         orders.add(order3);
         orders.add(order4);
 
         // In thông tin của các đối tượng Order2 ra console
         for (Order2 order : orders) {
             System.out.println(order.toString());
         }
 
         // Sử dụng debug để xem giá trị của các đối tượng thay đổi
         order1.setConfirm(false);
         order3.setPrice(3500);
 
         // In thông tin của các đối tượng Order2 sau khi thay đổi
         for (Order2 order : orders) {
             System.out.println(order.toString());
         }
     }
}
