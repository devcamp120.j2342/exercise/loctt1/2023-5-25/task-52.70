package models;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Oder2 {
     private int id;
     private String customerName;
     private Integer price;
     private Date orderDate;
     private Boolean confirm;
     private String[] items;
     private Person buyer;

     public void Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm, String[] items, Person buyer) {
          this.id = id;
          this.customerName = customerName;
          this.price = price;
          this.orderDate = orderDate;
          this.confirm = confirm;
          this.items = items;
          this.buyer = buyer;
      }
  
      public void Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm) {
          this.id = id;
          this.customerName = customerName;
          this.price = price;
          this.orderDate = orderDate;
          this.confirm = confirm;
      }
  
      public void Order2(int id, String customerName, Integer price, Boolean confirm, String[] items, Person buyer) {
          this.id = id;
          this.customerName = customerName;
          this.price = price;
          this.confirm = confirm;
          this.items = items;
          this.buyer = buyer;
      }
  
      public void Order2(int id, String customerName, Integer price, Boolean confirm) {
          this.id = id;
          this.customerName = customerName;
          this.price = price;
          this.confirm = confirm;
      }
  
      public void printOrder() {
          SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
  
          System.out.println("ID: " + id);
          System.out.println("Customer Name: " + customerName);
          System.out.println("Price: " + price + " VND");
          System.out.println("Order Date: " + dateFormat.format(orderDate));
          System.out.println("Confirm: " + confirm);
          System.out.println("Items: ");
          for (String item : items) {
              System.out.println("- " + item);
          }
          System.out.println("Buyer: " + buyer.getName());
      }
  
      @Override
      public String toString() {
          SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
  
          StringBuilder sb = new StringBuilder();
          sb.append("ID: ").append(id).append("\n");
          sb.append("Customer Name: ").append(customerName).append("\n");
          sb.append("Price: ").append(price).append(" VND").append("\n");
          sb.append("Order Date: ").append(dateFormat.format(orderDate)).append("\n");
          sb.append("Confirm: ").append(confirm).append("\n");
          sb.append("Items: ").append("\n");
          for (String item : items) {
              sb.append("- ").append(item).append("\n");
          }
          sb.append("Buyer: ").append(buyer.getName()).append("\n");
  
          return sb.toString();
      }
}
